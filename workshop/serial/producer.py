"""Most pointless producer-consumer ever."""
import queue
import time


def producer(items, count):
    for i in range(count):
        print('Producing', i)
        items.put(i)
        time.sleep(1)

    items.put(None)
    print('Producer done')


def consumer(items):
    while True:
        item = items.get()
        if item is None:
            break
        print('Consuming', item)
    print('Consumer done')


q = queue.Queue()
producer(q, 10)
consumer(q)
