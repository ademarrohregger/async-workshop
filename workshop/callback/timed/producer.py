from collections import deque

from .scheduler import Scheduler

sched = Scheduler()


class AsyncQueue:
    def __init__(self):
        self.items = deque()
        self.waiting = deque()

    def put(self, item):
        self.items.append(item)

        if self.waiting:
            func = self.waiting.popleft()
            # Let's avoid calling this directly, so we don't have long recursion
            # chains.
            sched.call_soon(func)

    def get(self, callback):
        if self.items:
            # This is the easy case.
            callback(self.items.popleft())
        else:
            # Now what? Well, guess we need to keep this guy for later.
            self.waiting.append(lambda: self.get(callback))


def producer(items, count):
    def _run(i):
        if i >= count:
            print('Producer done')
            items.put(None)
            return

        print('Producing', i)
        items.put(i)
        sched.call_later(1, lambda: _run(i + 1))

    _run(0)


def consumer(items):
    def _consume(item):
        if item is None:
            print('Consumer done')
            return

        print('Consuming', item)
        sched.call_soon(lambda: consumer(items))

    items.get(callback=_consume)


q = AsyncQueue()
sched.call_soon(lambda: producer(q, 10))
sched.call_soon(lambda: consumer(q))
sched.run()
