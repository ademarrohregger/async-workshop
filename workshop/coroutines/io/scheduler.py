import time
from collections import deque
from select import select

from ...util import PriorityQueue


class EmptyAwaitable:
    def __await__(self):
        yield


def switch():
    return EmptyAwaitable()


class Task:
    def __init__(self, sched, coro):
        self.sched = sched
        self.coro = coro

    def __call__(self):
        self.sched.current = self

        try:
            self.coro.send(None)
        except StopIteration:
            pass
        else:
            if self.sched.current is not None:
                self.sched.call_soon(self)


class AsyncSocket:
    def __init__(self, sched, sock):
        self.sched = sched
        self.sock = sock

    async def accept(self):
        await self.sched.wait_until_readable(self.sock)
        connection, address = self.sock.accept()
        return self.sched.make_async_socket(connection), address

    async def recv(self, maxbytes):
        await self.sched.wait_until_readable(self.sock)
        return self.sock.recv(maxbytes)

    async def send(self, buf):
        await self.sched.wait_until_writeable(self.sock)
        return self.sock.send(buf)

    def close(self):
        self.sock.close()


class Scheduler:
    def __init__(self):
        self.ready = deque()
        self.sleeping = PriorityQueue()
        self.sequence = 0
        self.current = None
        self._read_waiting = {}
        self._write_waiting = {}

    def call_soon(self, task):
        self.ready.append(task)

    def call_later(self, delay, callback):
        deadline = time.time() + delay
        self.sleeping.push((deadline, self.sequence, callback))
        self.sequence += 1

    def read_wait(self, f, callback):
        self._read_waiting[f] = callback

    def write_wait(self, f, callback):
        self._write_waiting[f] = callback

    def run(self):
        while self.ready or self.sleeping or self._read_waiting or self._write_waiting:
            if not self.ready:
                if self.sleeping:
                    deadline, _, func = self.sleeping.peek()
                    timeout = deadline - time.time()
                    if timeout < 0:
                        timeout = 0
                else:
                    timeout = None

                can_read, can_write, _ = select(
                    self._read_waiting, self._write_waiting, [], timeout
                )

                for fd in can_read:
                    self.ready.append(self._read_waiting.pop(fd))

                for fd in can_write:
                    self.ready.append(self._write_waiting.pop(fd))

                while self.sleeping:
                    deadline, _, func = self.sleeping.peek()
                    if time.time() > deadline:
                        self.ready.append(func)
                        self.sleeping.pop()
                    else:
                        break

            while self.ready:
                task = self.ready.popleft()
                task()

    def new_task(self, coro):
        self.call_soon(Task(self, coro))

    def release_current(self):
        callback = self.current
        self.current = None
        return callback

    async def sleep(self, delay):
        self.call_later(delay, self.release_current())
        await switch()

    async def wait_until_readable(self, f):
        self.read_wait(f, self.release_current())
        await switch()

    async def wait_until_writeable(self, f):
        self.write_wait(f, self.release_current())
        await switch()

    def make_async_socket(self, sock):
        return AsyncSocket(self, sock)
