from socket import AF_INET, SOCK_STREAM, socket

from .scheduler import Scheduler

sched = Scheduler()


async def echo_handler(sock):
    while True:
        data = await sock.recv(10000)
        if not data:
            break
        await sock.send(b'Got: ' + data)


async def tcp_server(addr):
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind(addr)
    sock.listen(1)
    async_sock = sched.make_async_socket(sock)

    while True:
        client, addr = await async_sock.accept()
        sched.new_task(echo_handler(client))


if __name__ == '__main__':
    sched.new_task(tcp_server(('', 30000)))
    sched.run()
