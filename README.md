Async Workshop
==============

This workshop material is _heavily_ "inspired" by David Beazley's [Build Your
Own Async](https://www.youtube.com/watch?v=Y4Gt3Xjd7G8)

For lecture notes, check the [script](./script.md).


Code examples
-------------

To run the examples, run them as Python modules, e.g.:

```bash
python -m workshop.coroutines.callbacks.progress
```


Licensing
---------

* Lecture notes are under [CC-BY-SA](https://creativecommons.org/licenses/)
* Code is under [MIT](./LICENSE.md)
